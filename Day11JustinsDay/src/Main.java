
public class Main {
	
	private static void main (String args[]){
		
		try{
			wakeUp();
			getsReadyForClass();
			goToClass();
			eatsLunch();
			staysAwake();
			goesHome();
		}catch(AbductionException e)
		{
			System.out.println("Justin heard about an abduction by an alien.");
			System.out.println("Justin goes back to sleep.");
		}
	}
	private static void wakeUp() throws AbductionException {
		System.out.println("Justin wakes up.");
	}
	
	private static void getsReadyForClass (){
		System.out.println("Justin gets ready for class.");
	}
	
	private static void goToClass(){
		System.out.println("Justin can't hear anything because of the fan.");
	}
	
	private static void eatsLunch(){
		System.out.println("Justin eats lunch. He has a McRib.");
	}
	
	private static void staysAwake(){
		System.out.println("Justin tries to stay awake in his later classes.");
	}
	
	private static void goesHome(){
		System.out.println("Just takes his hoverboard home.");
	}

}
