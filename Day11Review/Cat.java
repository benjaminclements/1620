/**
 * This class represents a cat.
 * 
 * Cat should inherit from AAnimal.
 * 
 * Cat should override the methods necessary to make this a concrete class.
 * 
 * A cats happiness improves only if it is fed cat food.
 * 
 * @author bricks
 *
 */

public class Cat extends AAnimal{
	
	@Override
	public string feed(IFood food) {
		if(food instanceof Catfood){
			increaseHappiness();
		}
	}

	

}
