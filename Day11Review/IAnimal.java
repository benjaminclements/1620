/**
 * This interface should contain exactly one method.
 * It should be called feed.
 * It shouldn't return anything.
 * It should take as a parameter, a reference called food of type IFood
 * @author bricks
 *
 */
public interface IAnimal {
	public static void feed(){
		
		void feed(IFood food);
		
	}
	
}
